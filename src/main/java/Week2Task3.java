/**
 * Created by oleg_nyus on Mar, 2019
 */
public class Week2Task3 {
  public static void main(String[] args) {

    //You have to strings "Cat" and "CAT" compare them ignoring case sensitivity

    String str1 = new String("CAT");
    String str2 = new String("cat");

    System.out.println("Comparing " + str1 + " and " + str2
            + " : " + str1.equalsIgnoreCase(str2));

  }
}
