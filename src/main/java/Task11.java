/**
 * Created by oleg_nyus on Mar, 2019
 */
public class Task11 {

  public static void main(String[] args) {

    // 1 - Print my name
    System.out.println("My name is Oleg.");

    // 2 - Calculate 45 + 78 / 87
    double x = 45.0 + 78.0 / 87.0;
    System.out.println(x);

    int y = 45 + 78 / 87;
    System.out.println(y);

    // 3 - Assign variable A to int 8 and then change it to String “8”
    int a = 8;
    String str1 = Integer.toString(a);
    System.out.println("This is a String: " + str1);

    // 4 - Assign Variable with your name and print string with replace “My name is ”
    String myName = "Oleg";
    System.out.println("My name is " + myName + ".");


    // 5 - If else. - Assign int variable.  If var is odd print “number is Odd”

    // 6 - Function create function that takes string argument and prints it

    // 7 - Return in function - the same as 6, but use return in function and call it.

    // 8 - Loops - create a function, which increment var x by 1 after each loop, and print value of x if x is less than 5
  }

}


