package Week31;

public class EvenNotEvenString {

    public static void main(String args[]){

        //count number of characters in a String
        String phrase = "Hello world";
        int length = phrase.length();
        System.out.println("The number is " + length);

        // Check if number of characters in a String is even or not even
        if ((length / 2) * 2 == length)
            System.out.println("String is even");
        else
            System.out.println("String is not even");

    }
}
