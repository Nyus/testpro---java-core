import java.util.Scanner;

/**
 * Created by oleg_nyus on Mar, 2019
 */
public class Week2Task5 {
  public static void main(String[] args) {

    // Write a program that prompts the user to input a positive integer.
    // It should then output a message indicating whether the number is a prime number or not.

    int c;
    System.out.println("Enter a number:");
    Scanner in = new Scanner(System.in);
    c = in.nextInt();

    boolean flag = false;
    for(int i = 2; i <= c/2; ++i)
    {
      if(c % i == 0)
      {
        flag = true;
        break;
      }
    }

    if (!flag)
      System.out.println(c + " is a prime number.");
    else
      System.out.println(c + " is not a prime number.");
  }
  }
