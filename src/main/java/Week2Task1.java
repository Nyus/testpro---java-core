/**
 * Created by oleg_nyus on Mar, 2019
 */
public class Week2Task1 {

  public static void main(String[] args) {

    //Replace following sentence with "Mercedes best german car" to "BMW best german car"

    String car1 = "Mercedes best german car";
    String car2 = car1.replace("Mercedes", "BMW");
    System.out.println(car2);
  }
}
