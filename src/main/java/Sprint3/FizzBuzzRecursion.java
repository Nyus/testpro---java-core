package Week3;

public class FizzBuzzRecursion {
    public static void main (String[] args) {
        recurse(1, 100);
    }

    // Define a range of recurse from int a to int b
    public static void recurse(int a, int b) {
        if(a <= b){
            if(a % 3 == 0 && a % 5 == 0){
                System.out.println("FizzBuzz");
            } else if(a % 3 == 0){
                System.out.println("Fizz");
            } else if(a % 5 == 0){
                System.out.println("Buzz");
            } else {
                System.out.println(a);
            }
            recurse(++a, b);
        } else {
            System.exit(0);
        }
    }
}
