package Sprint3;

/**
 * Created by oleg_nyus on Mar, 2019
 */
public class ClassTask1 {

  // INCAPSULATION ???
  // Create Class with integer attribute x equal to five,
  // Initialize an object of this class and call this object within the class and print integer

  int x = 5;
  public static void main(String args[]){

    ClassTask1 newObject = new ClassTask1();
    System.out.println(newObject.x);
  }
}

