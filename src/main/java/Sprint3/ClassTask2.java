package Sprint3;

/**
 * Created by oleg_nyus on Mar, 2019
 */
public class ClassTask2 {

  //Create one more Class, within the (main) class create static method to print “I am static method”
  public static void main(String[] args) {
    myMethod();
  }

  static void myMethod() {
    System.out.println("I am a static method");
  }
}
