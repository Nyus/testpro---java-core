package Sprint2;

/**
 * Created by oleg_nyus on Mar, 2019
 */
public class Palindrome {
  public static void main(String args[])
  {
    String reverse = "";
    String original = "dad";

    int length = original.length();

    for (int i = length - 1; i >= 0; i--)
      reverse = reverse + original.charAt(i);

    if (original.equals(reverse))
    //if (first.equalsIgnoreCase(reverse))
    System.out.println("The string " + "'" + original + "'" + " is a palindrome.");
        else
    System.out.println("The string " + "'" + original + "'" + " isn't a palindrome.");

  }
}
