package Sprint2;

public class PalindromeIgnoreCaseReverse {

    public static void main(String[] args){

        // In the below code, we are basically reading in a String from
        // the user afterwhich we will begin an iteration loop that will build the new reversed String.
        // This is done in the “for” loop by obtaining the characters of the original String
        // individually from the end by using the “charAt” function of the String class and concatenating them to a new String by using the “+” operator.
        String reverse = "";
        String origin = "151";

        // replaceAll("\\s+", "") - will replace all spaces with "" (nothing)
        // https://javarevisited.blogspot.com/2017/01/string-replaceall-example-how-to-replace-all-characters-and-substring.html
        String ignoreCase = origin.toLowerCase().replaceAll("\\s+", "");

        int length = ignoreCase.length();

        for (int i = length - 1; i>=0; i--)

            // https://beginnersbook.com/2013/12/java-string-charat-method-example/
            // charAt(5) will take 5th character in a String
            reverse = reverse + ignoreCase.charAt(i);

        if (ignoreCase.equals(reverse))
            System.out.println("The word " + "'" + origin + "'" + " is palindrome.");
        else
            System.out.println("The word " + "'" + origin + "'" + " is not palindrome");

    }
}
