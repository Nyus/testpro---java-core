package Sprint1;

import java.util.Scanner;

public class HomeWork1 {


    public static void main(String[] args) {

        // 1 - Print your name
        System.out.println("Oleg");

        // 2 - Calculate 45 + 78 / 87
        int a = 45 + 78 / 87;
        System.out.println(a);

        // 3 - Assign variable A to int 8 and then change it to String "8"
        int A = 8;
        String B = Integer.toString(A);
        System.out.println(B);

        // 4 - Assign variable with your name and print string with replaced "My name is "
        String myName = "Oleg";
        System.out.println("My name is " + myName);

        // 5 - If else - Assign int variable. If var is Odd print "Number is Odd."
        int c;
        System.out.println("Enter a number:");
        Scanner in = new Scanner(System.in);
        c = in.nextInt();

        if ((c / 2) * 2 == c)
            System.out.println("Number is Even");
        else
            System.out.println("Number is Odd");

        // 6 - Function - Create function that takes string argument and print it.
        myColor("Red");

        // 7 - Return in function - the same as 6, but use return in function and call it.
        myFavoriteDrink("Tea");


        // 8 - Loops - create a function, which increment var x by 1 after each loop, and print value of x if x is less than 5
        int maxNumber = 5;
        for (int i = 0; i < maxNumber; i++) {
            int numbers = i;
            System.out.println(numbers);
        }

    }

    // 6
    public static void myColor(String color) {
        System.out.println(color);
    }

    // 7
    public static String myFavoriteDrink(String drink) {
        System.out.println(drink);
        return drink;
    }

}




