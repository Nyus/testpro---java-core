/**
 * Created by oleg_nyus on Mar, 2019
 */
public class Week2Task4 {
  public static void main(String[] args) {

    //Write a program using switch statement to pick a Month by provided number from 1 to 12

    int month = 12;
    String monthString;

    switch (month) {
      case 1:
        monthString = "Jan";
        break;
      case 2:
        monthString = "Fab";
        break;
      case 3:
        monthString = "March";
        break;
      case 4:
        monthString = "Apr";
        break;
      case 5:
        monthString = "May";
        break;
      case 6:
        monthString = "June";
        break;
      case 7:
        monthString = "Jul";
        break;
      case 8:
        monthString = "Aug";
        break;
      case 9:
        monthString = "Sep";
        break;
      case 10:
        monthString = "Oct";
        break;
      case 11:
        monthString = "Nov";
        break;
      case 12:
        monthString = "Dec";
        break;
        default:
          monthString = "Invalid";
    }
      System.out.println(monthString);
    }

  }



 /* import java.time.Month;

class Playground {
  public static void main(String[ ] args) {
    int month = 6;

    if (month > 0 && month < 13 ) {
      System.out.println(Month.of(month));
    } else {
      System.out.println("Not a valid month");
    }
  }
}
*/